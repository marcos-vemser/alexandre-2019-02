
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        Dwarf novoAnao = new Dwarf("Dain");
        Elfo novoElfo = new Elfo("Legolas");
        novoElfo.atirarFlechaNoAnao(novoAnao);
        assertEquals(1,novoElfo.getExperiencia());
        assertEquals(3,novoElfo.getQtdFlecha());
        assertEquals(100.0, novoAnao.getvida(),0.1);
    }     
}
